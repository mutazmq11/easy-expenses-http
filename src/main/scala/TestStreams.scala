import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import com.typesafe.config.ConfigFactory

import scala.concurrent.{ExecutionContext, Future}
import ExecutionContext.Implicits.global

object TestStreams extends App {

  implicit val system = ActorSystem("reactive-tweets")
  implicit val materializer = ActorMaterializer()

  /*
  val source = Source(1 to 10)
  val sink = Sink.fold[Int, Int](0)(_ + _)
  val runnable: RunnableGraph[Future[Int]] = source.toMat(sink)(Keep.right)
  val future = runnable.run()

  //val future = source.runWith(sink)
  future.onSuccess{ case v =>
    println(v.toString)
  }
*/

  val runnable: RunnableGraph[Unit] = Source(1 to 6).via(Flow[Int].map(_ * 2)).to(Sink.foreach(println(_)))
  //Source(List(1,2,3)).map(_ * 2).to(Sink.foreach(println(_))).run()
  //val otherSink: Sink[Int, Unit] =
   // Flow[Int].alsoTo(Sink.foreach(println(_))).to(Sink.ignore)
}

