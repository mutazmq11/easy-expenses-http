package quotations

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import quotations.actors.ActorsManager
import quotations.actors.Messages._
import quotations.models.Quotation
import shared.utils.RouteDiscovery
import shared.utils.Routes

//import scala.concurrent.ExecutionContext.Implicits.global
/**
 * Created by mutaz on 10/25/15.
 */


trait QuotationRoutes extends ActorsManager with Routes{

  /**
   * To handle update, delete, details for a specific quotation
   */

  def crudOnQuotationRoutes: Route =
    path("quotations" / Segment) { id =>
    get {
      ctx => perQuotationRequestActor(new GetQuotation(id), ctx)
    } ~
      post {
        complete {
          "Received POST request for quotation " + id
        }
      } ~
      delete {
        complete {
          "Received delete request for quotation " + id
        }
      }
  }
  def listingOrNewQuotationRoutes = path("quotations") {
      get {
        complete {
          "Get list of all quotations"
        }
      } ~
      put {
        ctx => perQuotationRequestActor(NewQuotation, ctx)
      }
  }

  val r: Route = crudOnQuotationRoutes ~ listingOrNewQuotationRoutes

  registerRoute(r)
  /*
  def perReqActor(): Route{
    ctx =>
  }
  */


  /**
   * TODO merge routessss with routes
   */

  /*
  val routessss: Route = {
    logRequestResult("quotation-service") {
      pathPrefix("ip") {
        (get & path(Segment)) { ip =>
          complete {
            ""
          }
        } ~
          (post & entity(as[Quotation])) { quotation =>
            val dd = quotation.remove()
            onSuccess(dd){ case updateWriter =>
               val findResult = Quotation.find(BSONDocument())
               onSuccess(findResult){ case quotations =>
                  complete(quotations)
               }
            }
          }
          (post & entity(as[Quotation])) { quotation =>
            val ff = Quotation.insert(quotation)
            onSuccess(ff) { case _ =>
              complete(quotation)
            }
          }


          (post & entity(as[Quotation])) { quotation =>
            val dd = Quotation.find(BSONDocument())
            onSuccess(dd) { case quotations =>
              complete(quotations)
            }
          }

      }
    }
  } */
}