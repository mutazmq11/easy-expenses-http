package quotations.actors

import quotations.actors.Messages.{GetQuotation, RestMessage}
import akka.actor.{ActorRef, Props, ActorSystem}
import akka.http.scaladsl.server.{RouteResult, RequestContext}
import akka.stream.{ActorMaterializer, Materializer}
import akka.pattern.ask
import akka.util.Timeout
import shared.utils.ActorSys
import scala.concurrent.duration._

import scala.concurrent.{Future, ExecutionContextExecutor}

/**
 * Created by mutaz on 11/2/15.
 */
import ActorSys._

trait ActorsManager {

  val quotationActor = actorSystem.actorOf(Props[QuotationActor], "quotation-actor")

  def perQuotationRequestActor(message: RestMessage, ctx: RequestContext): Future[RouteResult] = {
    val actor = actorSystem.actorOf(PerRequestActor.props(quotationActor, ctx))
    /*
    * TODO we might need to move timeout to configuraitons
     */
    implicit val timeout = new Timeout(1000 seconds)

    (actor ? message ).mapTo[RouteResult]
  }

}
