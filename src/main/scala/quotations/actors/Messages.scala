package quotations.actors


import quotations.models.Quotation
import shared.utils.ExtendedJsonProtocol

/**
 * Created by mutaz on 11/2/15.
 */
object Messages {

  trait RestMessage

  case class GetQuotation(id: String) extends RestMessage

  case object NewQuotation extends RestMessage
}
