package quotations.actors

import akka.actor._
import Messages._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{RequestContext, RouteResult}
import shared.utils.Exceptions._
import scala.concurrent.duration._
import quotations.models.Quotation
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes._
import shared.utils.{MarshallableException, PerReq}

class PerRequestActor(target: ActorRef, ctx: RequestContext)
  extends PerReq(target: ActorRef, ctx: RequestContext) {

  import context._

  def waitingResponse: Receive = {

    case quotations: List[Quotation] =>
      completeReq(Marshal(quotations).to[ResponseEntity])

    case quotation: Quotation =>
      completeReq(Marshal(quotation).to[ResponseEntity])

    case ex: MarshallableException =>
      completeReq(ex.serialize, statusCode = BadRequest)

    case emptyList: List[Nothing] =>
      //initiator ! complete {"[]"}
      stop(self)

    case ReceiveTimeout   =>
      completeReq(TimeoutException("Request timeout").serialize, statusCode = GatewayTimeout)
      log.error(ReceiveTimeout.toString)
      /*
      * TODO Should log the requested data. Means the entity of ctx
      */
  }

}

object PerRequestActor {
  def props(target: ActorRef, ctx: RequestContext): Props = {
    Props(new PerRequestActor(target, ctx))
  }
}



//Legacy to checkout later when http allows for tell patter from the Route directly

/*
import akka.actor.{Props, ActorRef, ReceiveTimeout, Actor}
import Messages._
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ResponseEntity, StatusCode, HttpResponse}
import akka.http.scaladsl.server.{RouteResult, RequestContext}
import shared.utils.ExtendedJsonProtocol
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import scala.concurrent.Future
import scala.concurrent.duration._


class PerRequestActor(target: ActorRef, message: RestMessage, ctx: RequestContext) extends Actor with MessagesProtocol{

  import context._

  setReceiveTimeout(2.seconds)
  target ! message
  stop(self)

  def receive = {
    case res: Error => complete(OK, res)
    case v: Validation    => complete(BadRequest, v)
    case ReceiveTimeout   => complete(GatewayTimeout, Error("Request timeout"))
  }
  def complete[T <: AnyRef](status: StatusCode, obj: RestMessage) = {
    ctx.complete(status, obj)
    stop(self)
  }

}

object PerRequestActor {
  def props(target: ActorRef, message: RestMessage, ctx: RequestContext): Props = {
    Props(new PerRequestActor(target, message, ctx))
  }
}


*/









