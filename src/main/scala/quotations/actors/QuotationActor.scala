package quotations.actors

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{ResponseEntity, HttpEntity}
import akka.http.scaladsl.server.RequestContext
import akka.http.scaladsl.unmarshalling.Unmarshal
import quotations.actors.Messages.{NewQuotation, GetQuotation}
import akka.actor.{ActorRef, ActorLogging, Actor}
import quotations.models.Quotation
import reactivemongo.bson._
import shared.utils.ActorSys
import shared.utils.Exceptions.{DataErrorException, AlreadyHasIdException}
import scala.Option
import scala.util.{Success, Failure}
import spray.json.DefaultJsonProtocol
import quotations.models.Quotation._
import akka.http.scaladsl.marshalling.{Marshaller, Marshal}
import scala.concurrent.Future

/**
 * Created by mutaz on 11/2/15.
 */
import ActorSys._

class QuotationActor extends Actor with ActorLogging {

  /*
  TODO I'm not sure if GetQuotation is run concurrently. It doesn't seem it is benefiting from the Future!!
   Visit https://www.chrisstucchio.com/blog/2013/actors_vs_futures.html to learn more
   */

  def receive = {

    case (GetQuotation(id: String), ctx: RequestContext) =>
      extractandSendResut[Option[Quotation]](sender(), Quotation.getById(id) )

    case (NewQuotation, ctx: RequestContext) =>
      val s= sender()
      val quotationFuture =  Unmarshal(ctx.request.entity).to[Quotation]
      quotationFuture.onSuccess {
        case quotation =>
          if(quotation.id != None) {
          s ! new AlreadyHasIdException()
        } else {
            val q = quotation.copy(id = Some(BSONObjectID.generate))
            Quotation.insert(q).map(
              writeRestul =>
                if (writeRestul.ok)
                  s ! q
                else {
                  s ! DataErrorException(writeRestul.getMessage())
                  log.error(writeRestul.getStackTrace.toString)
                }
            )
          }
      }
      quotationFuture.onFailure {
        case e => s ! DataErrorException(e.getMessage)
      }
    case _ => log.info("got something")
  }

  def extractandSendResut[T](/*sender*/ s: ActorRef, future: Future[T]) = {

    future.onComplete{
      case Success(Some(e)) => s ! e
      case Success(e) => s ! e
      case Failure(ex) => s ! ex
    }
  }

}
