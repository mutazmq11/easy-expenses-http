package quotations.models

import akka.event.Logging
import com.typesafe.config.ConfigFactory
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.{MongoConnection, MongoDriver}

import scala.util.{Success, Try, Failure}

/**
 * Created by mutaz on 10/25/15.
 */
import scala.concurrent.ExecutionContext.Implicits.global


object DBConnection {

  val config = ConfigFactory.load()
  val dbURI = config.getString("mongodb.uri")
  val driver = new MongoDriver

  val conn: Try[MongoConnection] =
    MongoConnection.parseURI(dbURI).map { parsedUri =>
      driver.connection(parsedUri)
    }
  conn match {
    case Success(connection) =>
      None
    case Failure(e) =>
      throw new Exception("Unable to connect to db. Error message: " + e.toString)
  }
  val DB =  conn.get.db(config.getString("mongodb.db"))

}
