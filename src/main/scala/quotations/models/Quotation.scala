package quotations.models

import DBConnection._
import shared.utils.{ActorSys, ExtendedJsonProtocol}
import shared.utils.Exceptions._
import reactivemongo.api.ReadPreference
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.{UpdateWriteResult, WriteResult}
import reactivemongo.bson.Macros.Annotations.Key
import reactivemongo.bson._
import scala.concurrent.Future
import scala.util.{Success, Try, Failure}

//import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.ExecutionContext
import DBConnection._
/**
 * Created by mutaz on 10/25/15.
 */


case class Quotation(@Key("_id") id: Option[BSONObjectID], title: String, description: Option[String]){
  def update()(implicit ec: ExecutionContext): Future[UpdateWriteResult] = {
    Quotation.collection.update(BSONDocument("_id" -> this.id), this)
  }
  def remove()(implicit ec: ExecutionContext): Future[WriteResult] = {
    Quotation.collection.remove(BSONDocument("_id" -> this.id))
  }

}

trait QuotationProtocol extends ExtendedJsonProtocol {
  implicit val quotationHandler = Macros.handler[Quotation]
  implicit val quotationFormat = jsonFormat3(Quotation.apply)
}

object Quotation extends QuotationProtocol{

  val collection = DB.collection[BSONCollection]("quotations")

  def getById(id: String)(implicit ec: ExecutionContext): Future[Option[Quotation]]={
      //Try(this.get(BSONDocument("_id" -> BSONObjectID(id)))).recover{ case t => Future.failed(t) }.get
    //Try to parse bson id from string. This method return Try[BSONObjectId] and we can simple `match` them
    BSONObjectID.parse(id) match {

      // valid bson id
      case Success(bsonId) => this.get( BSONDocument("_id" -> bsonId) )

      //We catch IllegalArgumentException and just return None
      case Failure(ex) => Future.failed(new InvalidIdException(ex.getMessage))
    }
  }

  def get(query: BSONDocument)(implicit ec: ExecutionContext): Future[Option[Quotation]]={
      collection.find(query).one[Quotation](ReadPreference.Primary)
  }

  def find(query: BSONDocument)(implicit ec: ExecutionContext): Future[List[Quotation]]= {
    collection.find(query).cursor[Quotation](ReadPreference.Primary).collect[List]()
  }

  def insert(quotation: Quotation)(implicit ec: ExecutionContext): Future[WriteResult] = {
    collection.insert(quotation)
  }

}



