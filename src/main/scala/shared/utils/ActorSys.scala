package shared.utils

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.server.{RequestContext, RouteResult}
import akka.pattern.ask
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.Timeout
import quotations.actors.Messages.GetQuotation

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}

/**
 * Created by mutaz on 11/2/15.
 */
object ActorSys {

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher
  implicit val materializer: Materializer = ActorMaterializer()

}
