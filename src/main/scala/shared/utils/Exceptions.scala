package shared.utils

import akka.http.scaladsl.marshalling.{Marshaller, Marshal}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ResponseEntity
import spray.json.DefaultJsonProtocol

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
 * Created by mutaz on 11/14/15.
 */

object ExceptionsJsonProtocol extends DefaultJsonProtocol{
  implicit val invalidIDFormat = jsonFormat2(Exceptions.InvalidIdException.apply)
  implicit val timeoutFormat = jsonFormat2(Exceptions.TimeoutException.apply)
  implicit val alreadyHasIdFormat = jsonFormat2(Exceptions.AlreadyHasIdException.apply)
  implicit val dataErrorFormat = jsonFormat2(Exceptions.DataErrorException.apply)

}

// An abstraction to avoid multiple match/case statements for different validation exceptions
trait MarshallableException{
  def serialize: Future[ResponseEntity]
}

object Exceptions {
  import ExceptionCodes._
  import ExceptionsJsonProtocol._

  case class TimeoutException(error: String, code: String = timeout) extends Exception with MarshallableException {
    override def serialize: Future[ResponseEntity] = {
      Marshal(this).to[ResponseEntity]
    }
  }

  case class InvalidIdException(error: String, code: String = invalidId) extends Exception with MarshallableException {
    override def serialize: Future[ResponseEntity] = {
      Marshal(this).to[ResponseEntity]
    }
  }

  case class AlreadyHasIdException(error: String = "Object already has an ID and can not be created as a new object", code: String = alreadyHasID)
    extends Exception with MarshallableException {
    override def serialize: Future[ResponseEntity] = {
      Marshal(this).to[ResponseEntity]
    }
  }

  case class DataErrorException(error: String, code: String = dataError)
    extends Exception with MarshallableException {
    override def serialize: Future[ResponseEntity] = {
      Marshal(this).to[ResponseEntity]
    }
  }

}


object ExceptionCodes {
  val invalidId = "6001"
  val alreadyHasID = "6002"
  val dataError = "6003"


  val timeout = "7001"
}

