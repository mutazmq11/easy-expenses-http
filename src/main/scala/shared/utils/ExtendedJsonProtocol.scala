package shared.utils

import reactivemongo.bson.BSONObjectID
import spray.json._

/**
 * Created by mutaz on 10/27/15.
 */

trait ExtendedJsonProtocol extends DefaultJsonProtocol {

  implicit object BSONObjectIDJsonFormat extends JsonFormat[BSONObjectID] {
    def write(id: BSONObjectID) = JsString(id.stringify)
    def read(value: JsValue) = value match {
      case JsString(x) => BSONObjectID(x)
      case x => deserializationError("Expected BSONObjectID as JsString, but got " + x)
    }
  }
}

