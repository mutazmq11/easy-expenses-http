package shared.utils

import akka.actor.{ActorRef, ActorLogging, Actor}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model.{ContentTypes, HttpResponse, StatusCode, ResponseEntity}
import akka.http.scaladsl.server.{RequestContext, RouteResult}
import quotations.actors.Messages.RestMessage
import scala.concurrent.duration._

import scala.concurrent.Future

/**
 * Created by mutaz on 11/14/15.
 */
abstract class PerReq(target: ActorRef, ctx: RequestContext) extends Actor with ActorLogging{
  import context._

  /*
  * TODO We might need to move all timoue out to a configuration file to unify the durations and control
 */
  setReceiveTimeout(10.seconds)

  var initiator: ActorRef = _

  def waitingResponse: Receive

  def receive = {
    case msg: RestMessage =>
      initiator = sender()
      target ! (msg, ctx)
      become(waitingResponse, true)
  }


  def completeReq(entityFutre: Future[ResponseEntity], statusCode: StatusCode = OK, isStop: Boolean = true): Unit = {
    import scala.util.{Success, Failure}
    entityFutre.onComplete {
      case Success(e) =>
        initiator ! RouteResult.Complete(new HttpResponse(status = statusCode, entity = e.withContentType(ContentTypes.`application/json`)))
      // A case where it is validation error and should be returned to the client
      case Failure(f: MarshallableException) =>
        f.serialize.onComplete{
          case Success(e) =>
            initiator ! RouteResult.Complete(new HttpResponse(status = InternalServerError, entity = e))
          case Failure(ff) =>
            log.error(f.toString)
        }
      // A case where it is
      case Failure(f) =>
        initiator ! RouteResult.Complete(new HttpResponse(status = InternalServerError,entity = f.toString))
        log.error(f.getStackTrace.toString)
    }
    if(isStop)
      stop(self)
  }
}
