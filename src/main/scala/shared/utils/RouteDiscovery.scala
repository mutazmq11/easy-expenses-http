package shared.utils

import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._

/**
 * Created by mutaz on 10/29/15.
 */
trait RouteDiscovery {
  implicit def rejectionHandler = RejectionHandler.newBuilder()
  .handleAll[MethodRejection] { rejections =>
    val methods = rejections map (_.supported)
    lazy val names = methods map (_.name) mkString ", "

    respondWithHeader(Allow(methods)) {
      options {
        complete(s"Supported methods : $names.")
      } ~
      complete(MethodNotAllowed,
        s"HTTP method not allowed, supported methods: $names!")
    }
  }
    .result()
}
