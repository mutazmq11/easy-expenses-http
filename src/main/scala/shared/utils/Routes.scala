package shared.utils

import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._

/**
 * Created by mutaz on 11/10/15.
 */
trait Routes extends RouteDiscovery {

  var routes: Option[Route] = None: Option[Route]

  def registerRoute(r: Route): Unit ={
    routes match {
      case None => routes = Some(r)
      case Some(rr) => routes = Some(rr ~ r)
    }
  }
  def getRoutes(): Route = {
    routes match {
      case Some(route) => route
      case None => get {
        complete("No routes defined")
      }
    }
  }
}
