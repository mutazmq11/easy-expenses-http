package shared.utils

import scala.util.{Failure, Success}

import akka.http.scaladsl.marshalling.{Marshaller, Marshal}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ResponseEntity
import spray.json.{DefaultJsonProtocol}
import DefaultJsonProtocol._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


object SomeObject extends DefaultJsonProtocol{
  implicit val someClassFormat = jsonFormat2(Excs.SomeClass.apply)
}


trait MarshallableException1{
  def serialize: Future[ResponseEntity]
}

object Excs {
  case class SomeClass(msg:String, code: String = "6001") extends MarshallableException1 {
    import SomeObject._
    def serialize: Future[ResponseEntity] = {
      Marshal(this).to[ResponseEntity]
    }
  }

}



object Test extends App{

  println("Start...\n")
  val ex = new Excs.SomeClass("Test exception")
  ex.serialize.onComplete{
    case Success(e) =>
      println(e.toString())
    case Failure(f) =>
      println("Serialization Failure " + f.printStackTrace())
  }

  println("Start...\n")
  /*
  val ex = new InvalidID("Test exception")
  println("Start...\n")
  ex.serialize.onComplete{
    case Success(e) => println(e.toString())
    case Failure(f) =>
      println("Serialization Failure " + f.printStackTrace())
  }*/
}
