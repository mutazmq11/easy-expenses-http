import akka.testkit.TestProbe
import org.scalatest.concurrent.AsyncAssertions
import quotations.QuotationRoutes
import reactivemongo.bson.BSONObjectID
import shared.utils.{Routes, ActorSys}
import akka.event.NoLogging
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.{HttpResponse, HttpRequest}
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.{RouteTestTimeout, ScalatestRouteTest}
import akka.stream.scaladsl.Flow
import org.scalatest._
import quotations.models.{Quotation, QuotationProtocol}
import org.scalatest.concurrent.Eventually._
import scala.concurrent.duration._
import akka.testkit._

//trait DummyTrait extends QuotationRoutes

class ServiceSpec extends FlatSpec with Matchers with ScalatestRouteTest with QuotationRoutes {

  val fullQuotation = Quotation(None, "Quotation title", Option("quotation descritpion"))
  //val quotationWithId = Quotation("IDIDIDIDID", "Quotation title", "quotation descritpion")

  val getQuotation = Quotation(Option(BSONObjectID("5658d3670f00000f0077966a")), "Syria Flight Quotation11 - updatedddd453", Option("descccc3311"))
  //implicit val defaultTimeout = RouteTestTimeout(5.seconds.dilated)

  /*
  * TODO the below line could help resolve the conflict of system so that we can return to ActorSys inheritance rather than import
   * implicit def default(implicit system: ActorSystem) = RouteTestTimeout(new DurationInt(5).second.dilated(system))
   */


  "Service" should "respond to single IP query" in {
    Get("/quotations/5658d3670f00000f0077966a") ~> getRoutes() ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[Quotation] shouldBe getQuotation
    }
  }
/*
    Get(s"/ip/${ip2Info.ip}") ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[IpInfo] shouldBe ip2Info
    }
  }

  it should "respond to IP pair query" in {
    Post(s"/ip", IpPairSummaryRequest(ip1Info.ip, ip2Info.ip)) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[IpPairSummary] shouldBe ipPairSummary
    }
  }
  */
/*
  it should "respond with bad request on incorrect IP format" in {
    Get("/ip/asdfg") ~> routes ~> check {
      status shouldBe BadRequest
      responseAs[String].length should be > 0
    }

    Post(s"/ip", IpPairSummaryRequest(ip1Info.ip, "asdfg")) ~> routes ~> check {
      status shouldBe BadRequest
      responseAs[String].length should be > 0
    }

    Post(s"/ip", IpPairSummaryRequest("asdfg", ip1Info.ip)) ~> routes ~> check {
      status shouldBe BadRequest
      responseAs[String].length should be > 0
    }
  }
  */
}
